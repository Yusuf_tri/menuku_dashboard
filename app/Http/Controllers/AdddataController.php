<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Carbon\Carbon;
use Datetime;
use Storage;
use App\Models\Resto;
use App\Models\Owner;
use App\Models\Users;

class AdddataController extends Controller
{
    public function tambahcabang(Request $request)
	{
		$addcabang = New Resto;
		$addcabang ->name_resto = $request->nama_resto."_".$request->nama_cabang;
		$addcabang ->address_resto = $request->alamat;
		$addcabang ->phone_resto = $request->telepon;
		$addcabang ->save();
		return redirect('cabangbaru');
	}	
	
	public function tambahuser(Request $request)
	{
		$adduser = New Users;
		$adduser ->user_id = strtoupper($request->user_id);
		$adduser ->user_pass = $request->user_pwd;
		$adduser ->user_phone = $request->user_phone;
		$adduser ->user_email = $request->user_email;
		$adduser ->user_level = $request->user_level;
		$adduser ->user_status = 1;
		$adduser ->save();
		return redirect('userbaru');
	}	
	
	public function tambahowner(Request $request)
	{
		$id = $request->user_id;
		$data = Users::where('user_id',$id)->first();	
		$addresto = New Resto;
		$addresto ->name_resto = $request->resto_name;
		$addresto ->address_resto = $request->resto_address;
		$addresto ->phone_resto = $request->resto_telp;
		$addresto ->user_created = $data->id_user;
		$addresto ->save();
		
		$addowner = New Owner;
		$addowner ->owner_name = $request->owner_name;
		$addowner ->owner_phone = $request->owner_phone;
		$addowner ->owner_email = $request->owner_email;
		$addowner ->owner_nik = $request->owner_nik;
		$addowner ->owner_npwp = $request->owner_npwp;
		$addowner ->owner_address = $request->owner_addess;
		$addowner ->user_created = $data->id_user;
		$addowner ->save();
		return redirect('dataresto');
		
	}	
}
