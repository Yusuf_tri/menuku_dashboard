<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\RedirectResponse;
use App\Models\Resto;
use App\Models\Owner;
use App\Models\Users;

class MenukuController extends Controller
{
    public function login()
	{
		return view ('welcome');
	}
	
	public function logout(Request $request)
	{
        $id = $request->id;
		$request->session()->forget('user_id',$id);
        return view ('welcome');
    }
	
	public function dashboard(Request $request)
	{
		$id = $request->id;
        $password = $request->password;
		
			$data = Users::where('user_id',$id)->first();
			if($data)
			{
				if($data->user_pass==$password)
				{
					Session::put('id_user',$data->id_user);
					Session::put('user_id',$data->user_id);
					Session::put('user_pass',$data->user_pass);
					Session::put('user_phone',$data->user_phone);
					Session::put('user_email',$data->user_email);
					Session::put('login',TRUE);
					if($data->user_level==2)
					{	
					return view ('dashboard'); 
					}
					else if ($data->user_level==1)
					{
					return 'Halaman Administrator';
					}
					else  if ($data->user_level==3)
					{
					return 'Halaman User';	
					}		
				}
				 else
				{
					return view ('welcome');
				}	 
			}
			else
			{
			return view ('welcome');
			}		
	}
	public function orderlist()
	{
		return view ('listorder');
	}
	
	public function preparelist()
	{
		return view ('listprepared');
	}
	public function dataresto()
	{
		
	if (Session::has('user_id'))
	{
		$id = Session::get('user_id');
		$dataresto = Users::Where('user_id',$id)
            ->join('resto', 'users.id_user','=','resto.user_created')
			->select('resto.name_resto','resto.address_resto','resto.phone_resto')
            ->get();
		//echo $dataresto;	
		return view ('addresto',compact('dataresto'));
		}
		else
		{
		return view ('welcome');
		}
		
	}
	
	public function cabangbaru()
	{
		$datacabang=Resto::all();
		return view ('addcabang',compact('datacabang'));
	}
	
	public function tambahuser()
	{
		$datauser=Users::all();
		return view ('adduser',compact('datauser'));
	}
	
	public function tambahowner()
	{
		$dataresto=Resto::all();
		$dataowner=Owner::all();
		return view ('addowner',compact('dataowner',));
	}
}
