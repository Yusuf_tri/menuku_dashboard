<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/* View */
Route::get('/','MenukuController@login');
Route::post('/dashboard','MenukuController@dashboard');
Route::get('/order','MenukuController@orderlist');
Route::get('/onprepared','MenukuController@preparelist');
Route::get('/dataresto','MenukuController@dataresto');
Route::get('/cabangbaru','MenukuController@cabangbaru');
Route::get('/userbaru','MenukuController@tambahuser');
Route::get('/logout','MenukuController@logout');
/*Data Processing*/
Route::get('/addcabang','AdddataController@tambahcabang');
Route::get('/adduser','AdddataController@tambahuser');
//Route::get('/addowner','AdddataController@tambahowner');
Route::post('/addrestonowner','AdddataController@tambahowner');
