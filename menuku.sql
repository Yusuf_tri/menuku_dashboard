/*
MySQL Data Transfer
Source Host: localhost
Source Database: menuku
Target Host: localhost
Target Database: menuku
Date: 13/04/2021 09:16:53
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_resto` int(11) DEFAULT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_price` int(10) unsigned DEFAULT NULL,
  `menu_detail` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for owner
-- ----------------------------
DROP TABLE IF EXISTS `owner`;
CREATE TABLE `owner` (
  `id_owner` int(11) NOT NULL AUTO_INCREMENT,
  `owner_name` varchar(500) DEFAULT NULL,
  `owner_phone` varchar(13) DEFAULT NULL,
  `owner_email` varchar(500) DEFAULT NULL,
  `owner_nik` varchar(20) DEFAULT NULL,
  `owner_npwp` varchar(20) DEFAULT NULL,
  `owner_address` varchar(500) DEFAULT NULL,
  `user_created` varchar(500) DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_owner`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for resto
-- ----------------------------
DROP TABLE IF EXISTS `resto`;
CREATE TABLE `resto` (
  `id_resto` int(11) NOT NULL AUTO_INCREMENT,
  `name_resto` varchar(500) DEFAULT NULL,
  `address_resto` varchar(500) DEFAULT '',
  `phone_resto` varchar(13) DEFAULT NULL,
  `resto_table_total` int(3) DEFAULT NULL,
  `user_created` varchar(500) DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_resto`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(200) DEFAULT '',
  `user_pass` varchar(100) DEFAULT '',
  `user_phone` varchar(15) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_status` int(3) NOT NULL,
  `user_level` int(3) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `owner` VALUES ('1', 'Joseph', '0853433234234', 'owner@arayaresto.com', '3502903003030', '14232323232', 'Jl Golf 4 f', '2', '2021-04-08 16:37:50', '2021-04-08 16:37:50');
INSERT INTO `owner` VALUES ('2', 'Joseph', '0853433234234', 'owner@arayacotage.com', '3502903003030', '14232323232', 'Jl.Bukit Golf Blok A4 Malang', '2', '2021-04-09 02:44:39', '2021-04-09 02:44:39');
INSERT INTO `owner` VALUES ('3', 'Rolando', '081234987621', 'owner@bofaloresto.com', '35983998839', '14400999292020', 'Jl.Bukit Golf 7C Araya Malang', '3', '2021-04-09 03:45:47', '2021-04-09 03:45:47');
INSERT INTO `owner` VALUES ('4', 'Joseph', '0853433234234', 'owner@bofalovillageresto.com', '3502903003030', '14232323232', 'Jl.Bukit Golf Blok 5 A Araya - Malang', '3', '2021-04-09 06:09:35', '2021-04-09 06:09:35');
INSERT INTO `owner` VALUES ('5', 'Rolando', '0853433234234', 'owner@arayavillageresto.com', '3502903003030', '14400999292020', 'Jl. Bukit Golf Blok A 5 Malang', '2', '2021-04-12 03:40:07', '2021-04-12 03:40:07');
INSERT INTO `resto` VALUES ('1', 'ARAYA RESTO', 'Jl.ARAYA MAlang', '0341-893403', null, '2', '2021-04-08 16:37:50', '2021-04-08 16:37:50');
INSERT INTO `resto` VALUES ('2', 'ARAYA Cotage', 'Jl. Bukit Golf Blok A 5', '0341-893506', null, '2', '2021-04-09 02:44:39', '2021-04-09 02:44:39');
INSERT INTO `resto` VALUES ('3', 'BOfalo Resto', 'Jl. Ijen 50 A Malang', '0341-899388', null, '3', '2021-04-09 03:44:58', '2021-04-09 03:44:58');
INSERT INTO `resto` VALUES ('4', 'BOfalo Cotage', 'Jl. Bukit Golf 5 A Malang', '0341-899386', null, '3', '2021-04-09 03:45:47', '2021-04-09 03:45:47');
INSERT INTO `resto` VALUES ('5', '_', null, null, null, '', '2021-04-09 04:11:27', '2021-04-09 04:11:27');
INSERT INTO `resto` VALUES ('6', '_', null, null, null, '', '2021-04-09 04:11:38', '2021-04-09 04:11:38');
INSERT INTO `resto` VALUES ('7', 'Bofalo Village Resto', 'Rest Area Tol Malang - Surabaya Km 50 Pandaan', '0343- 980930', null, '3', '2021-04-09 06:09:35', '2021-04-09 06:09:35');
INSERT INTO `resto` VALUES ('8', 'Araya Village Resto', 'Rest Area Tol Malang - Surabaya KM 50 Pandaan', '343-98939388', null, '2', '2021-04-12 03:40:07', '2021-04-12 03:40:07');
INSERT INTO `users` VALUES ('1', 'YUSUF', 'Yusuf123', '085933737609', 'yusuftri@redision.com', '1', '1', '2021-04-08 13:28:32', '2021-04-08 13:28:32');
INSERT INTO `users` VALUES ('2', 'ARAYA_RESTO', 'Arayaresto', '08938300393', 'admin@arayaresto.com', '1', '2', '2021-04-08 13:39:28', '2021-04-08 13:39:28');
INSERT INTO `users` VALUES ('3', 'BOFALO_RESTO', 'Bofaloresto', '089376828877', 'admin@bofaloresto.com', '1', '2', '2021-04-09 03:41:59', '2021-04-09 03:41:59');
